#!/bin/bash
for a in flavi dengue hcv; do 
    gunzip $a.cm
    gunzip $a.cm.i1f
    gunzip $a.cm.i1i
    gunzip $a.cm.i1m
    gunzip $a.cm.i1p
    gunzip $a.pt.hmm
    gunzip $a.pt.hmm.h3f
    gunzip $a.pt.hmm.h3i
    gunzip $a.pt.hmm.h3m
    gunzip $a.pt.hmm.h3p
done
