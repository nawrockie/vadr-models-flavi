#!/bin/bash
for a in flavi dengue hcv; do
    gzip $a.cm
    gzip $a.cm.i1f
    gzip $a.cm.i1i
    gzip $a.cm.i1m
    gzip $a.cm.i1p
    gzip $a.pt.hmm
    gzip $a.pt.hmm.h3f
    gzip $a.pt.hmm.h3i
    gzip $a.pt.hmm.h3m
    gzip $a.pt.hmm.h3p
done
