March 2021
vadr-models-flavi-1.2-1
https://bitbucket.org/nawrockie/vadr-models-flavi

Flaviviridae virus models for use with vadr, version
1.2 (and possibly later versions).

If gzipped files (file names ending in .gz, e.g. flavi.cm) exist in
this directory, execute the gunzip.sh script to unpack them:
> sh ./gunzip.sh

If you downloaded this file as a gzipped tarball, the .gz files should
already be unpacked after extracting the tarball.

---

VADR documentation can be found here:
https://github.com/nawrockie/vadr/blob/master/README.md

See RELEASE-NOTES.txt for details on changes between model versions. 

---

Contact eric.nawrocki@nih.gov for help.
